package nielskooij.aads;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WinnerTest {

    @Test
    public void testMark() {
        assertEquals("Mark", Winner.Mark.toString());
    }

    @Test
    public void testVeronique() {
        assertEquals("Veronique", Winner.Veronique.toString());
    }

}
