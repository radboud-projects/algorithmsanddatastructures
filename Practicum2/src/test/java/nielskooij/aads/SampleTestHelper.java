package nielskooij.aads;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class SampleTestHelper {

    private static File sampleDir = new File(SampleTestHelper.class.getResource("/samples").getFile());

    public static InputStream getSampleInput(String name) throws FileNotFoundException {
        return new FileInputStream(new File(sampleDir, name + ".in"));
    }

    public static InputStream getSampleOutput(String name) throws FileNotFoundException {
        return new FileInputStream(new File(sampleDir, name + ".out"));
    }

    public static Set<String> getSampleNames() {
        Set<String> result = new HashSet<>();
        for(File f : sampleDir.listFiles()) {
            result.add(f.getName().substring(0, f.getName().lastIndexOf(".")));
        }
        return result;
    }

}
