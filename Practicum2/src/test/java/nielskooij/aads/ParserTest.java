package nielskooij.aads;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class ParserTest {

    private Parser parser;

    @Before
    public void setup() {
        parser = null;
    }

    @Test
    public void testInput() throws IOException {
        parser = new Parser(SampleTestHelper.getSampleInput("sample-A.1"));
        boolean[][] g = parser.parseInput();

        boolean[][] expected = new boolean[][]{
                new boolean[]{true, true},
                new boolean[]{true, false}
        };

        assertEquals(2, g.length);

        for(int i = 0; i < 2; i++) {
            for( int j = 0; j < 2; j++) {
                assertEquals("Index: " + i + "," + j, expected[i][j], g[i][j]);
            }
        }
    }

    @Test
    public void testOutputMark() throws IOException {
        parser = new Parser(SampleTestHelper.getSampleOutput("sample-A.1"));

        assertOutput(parser, Winner.Mark);
    }

    @Test
    public void testOutputVeronique() throws IOException {
        parser = new Parser(SampleTestHelper.getSampleOutput("sample-A.2"));

        assertOutput(parser, Winner.Veronique);
    }

    private void assertOutput(Parser parser, Winner expected) throws IOException {
        Winner actual = parser.parseOutput();

        assertEquals(expected, actual);
    }
}
