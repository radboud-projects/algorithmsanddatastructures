package nielskooij.aads.algorithm;

import nielskooij.aads.Parser;
import nielskooij.aads.SampleTestHelper;
import nielskooij.aads.Winner;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class BiPartiteAlgorithmSampleTest {

    private BiPartiteAlgorithm algorithm;

    @Before
    public void setup() {
        algorithm = new BiPartiteAlgorithm();
    }

    @Test
    public void testA() throws IOException {
        testX("a", 3);
    }

    @Test
    public void testM() throws IOException {
        testX("m", 5);
    }

    @Test
    public void testR() throws IOException {
        testX("r", 11);
    }

    @Test
    public void testV() throws IOException {
        testX("v", 5);
    }

    @Test
    public void testSamplesA() throws IOException {
        testSamplesX("A", 10);
    }

    @Test
    public void testSamplesB() throws IOException {
        testSamplesX("B", 11);
    }

    private void testSamplesX(String s, int amount) throws IOException {
        String file = "sample-" + s + ".";

        for(int i = 1; i < amount; i++) {
            test(file + i);
        }
    }

    private void testX(String s, int amount) throws IOException {
        for(int i = 1; i < amount; i++) {
            test(s + i);
        }
    }

    private void test(String filename) throws IOException {
        long timestamp = System.currentTimeMillis();
        boolean[][] g = new Parser(SampleTestHelper.getSampleInput(filename)).parseInput();
        Winner actual = algorithm.calculateWinner(g, g.length);

        Winner expected = new Parser(SampleTestHelper.getSampleOutput(filename)).parseOutput();
        assertEquals(filename + " Failed", expected, actual);
        System.out.println(filename + " passed in " + (System.currentTimeMillis() - timestamp) + " ms");
    }

}
