package nielskooij.aads.algorithm;

import nielskooij.aads.Winner;

public class BiPartiteAlgorithm {

    public Winner calculateWinner(boolean[][] graph, int n) {
        // If all vertices can be matched Mark will have the last move and will win.
        // If they cannot all be matched Veronique will win.
        return maxBPM(graph, n) == n ? Winner.Mark : Winner.Veronique;
    }

    /**
     * Runs maximal bipartite matching on the graph.
     * @param graph
     * @param n The size of the graph.
     * @return The number of vertices that can be matched.
     */
    private int maxBPM(boolean[][] graph, int n) {
        int[] matchR = new int[n];

        for(int i = 0; i < n; i++) {
            matchR[i] = -1;
        }

        int result = 0;
        for(int u = 0; u < n; u++) {
            boolean[] seen = new boolean[n];

            if(bpm(graph, u, seen, matchR, n))
                result++;
        }

        return result;
    }

    /**
     * Tries to find a match for vertex u.
     * @param graph
     * @param u
     * @param seen
     * @param matchR
     * @param n
     * @return
     */
    private boolean bpm(boolean[][] graph, int u, boolean[] seen, int[] matchR, int n) {
        for(int v = 0; v < n; v++) {
            if(graph[u][v] && !seen[v]) {
                seen[v] = true;

                if(matchR[v] < 0 || bpm(graph, matchR[v], seen, matchR, n)) {
                    matchR[v] = u;
                    return true;
                }
            }
        }

        return false;
    }
}
