package nielskooij.aads;

import nielskooij.aads.algorithm.BiPartiteAlgorithm;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        Parser parser = new Parser(System.in);
        boolean[][] graph = parser.parseInput();

        BiPartiteAlgorithm algorithm = new BiPartiteAlgorithm();
        Winner winner = algorithm.calculateWinner(graph, graph.length);
        System.out.println(winner);
    }

}
