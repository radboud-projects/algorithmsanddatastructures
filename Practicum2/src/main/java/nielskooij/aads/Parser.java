package nielskooij.aads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Parser {

    private BufferedReader reader;
    private Map<String, Integer> actorIdMap = new HashMap<>();
    private Map<String, Integer> actressIdMap = new HashMap<>();
    private int currentId = 0;

    public Parser(InputStream input) {
        reader = new BufferedReader(new InputStreamReader(input));
    }

    /**
     * Will parse the input and return a graph represented as a boolean matrix
     * @return
     * @throws IOException When reading the input fails
     */
    public boolean[][] parseInput() throws IOException {
        int[] firstLine = parseFirstLine();
        int n = firstLine[0];
        int m = firstLine[1];

        boolean[][] bipartiteGraph = new boolean[n][n];

        readActors(n, false);
        readActors(n, true);

        for(int i = 0; i < m; i++) {
            parseMovie(bipartiteGraph);
        }

        return bipartiteGraph;
    }

    /**
     * Parse the first line of the input.
     * This will return an array with the amount of actors and actresses
     * as first element and the amount of movies as second element.
     * @return
     * @throws IOException
     */
    private int[] parseFirstLine() throws IOException {
        String line = reader.readLine();
        String[] parts = line.split(" ");
        return new int[]{ Integer.parseInt(parts[0]), Integer.parseInt(parts[1]) };
    }

    /**
     * Reads an actor or actress and stores it so we can use it to build the graph later.
     * @param amount
     * @param isActor
     * @throws IOException
     */
    private void readActors(int amount, boolean isActor) throws IOException {
        currentId = 0;
        String currentLine;
        while(amount > 0 && (currentLine = reader.readLine()) != null) {
            if(isActor) {
                actorIdMap.put(currentLine, currentId);
            } else {
                actressIdMap.put(currentLine, currentId);
            }
            currentId++;
            amount--;
        }
    }

    /**
     * Parses a movie and updates the graph accordingly.
     * @param graph
     * @throws IOException
     */
    private void parseMovie(boolean[][] graph) throws IOException {
        String currentLine = reader.readLine(); //We don't care about the name of the movie.
        currentLine = reader.readLine();
        int nrOfPairs = Integer.parseInt(currentLine);

        List<String> actors = new ArrayList<>();
        List<String> actresses = new ArrayList<>();

        while(nrOfPairs > 0 && (currentLine = reader.readLine()) != null) {
            Integer src = actorIdMap.get(currentLine);
            boolean isSrcActor = src != null;

            if(!isSrcActor) src = actressIdMap.get(currentLine);

            if(isSrcActor) {
                for (String actress : actresses) {
                    int destination = actressIdMap.get(actress);
                    graph[destination][src] = true;
                }
                actors.add(currentLine);
            } else {
                for (String actor : actors) {
                    int destination = actorIdMap.get(actor);
                    graph[src][destination] = true;
                }

                actresses.add(currentLine);
            }
            nrOfPairs--;
        }
    }

    public Winner parseOutput() throws IOException {
        return Winner.valueOf(reader.readLine());
    }

}
