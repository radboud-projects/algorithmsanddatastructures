package nielskooij.aads;

import nielskooij.aads.graph.UndirectedGraph;
import nielskooij.aads.graph.Vertex;

import java.awt.*;
import java.util.*;
import java.util.List;

public class PiAlgorithm {

    private List<Integer> subPathLengths = new ArrayList<>();
    private List<Vertex> cacheBFS = new ArrayList<>();

    public int calculateLongestPath(UndirectedGraph g) {
        pathAlgorithm(g);

        Collections.sort(subPathLengths, (i, i1) -> {
            if(i == i1) return 0;
            else if(i > i1) return -1;
            return 1;
        });

        int totalLongest = 0;
        if(subPathLengths.size() == 1) {
            totalLongest = Math.max(0, subPathLengths.get(0) - 1);
        }else if (subPathLengths.size() == 2) {
            int longest = subPathLengths.get(0);
            int second = subPathLengths.get(1);

            totalLongest = (int) (Math.ceil(longest / 2.0) + Math.ceil(second / 2.0));
        }else {
            int longestHalf = (int) Math.ceil(subPathLengths.get(0) / 2.0);
            int secondLongestHalf = (int) Math.ceil(subPathLengths.get(1) / 2.0);
            int thirdLongestHalf = (int) Math.ceil(subPathLengths.get(2) / 2.0);

            int candidateOne = longestHalf + secondLongestHalf;
            int candidateTwo = secondLongestHalf + 1 + thirdLongestHalf;

            totalLongest = Math.max(candidateOne, candidateTwo);
        }

        return Math.max(totalLongest, subPathLengths.get(0) - 1);
    }

    /**
     * Calculate all the longest paths from subcomponents in the graph.
     * @param g
     */
    public void pathAlgorithm(UndirectedGraph g) {
        for(Vertex v : g.getVertices()) {
            if(!v.isVisited()) {
                Vertex last = BFS(v);
                Vertex longestPathLast = last;
                if(!last.equals(v)) {
                    longestPathLast = BFS(last);
                }

                // path length excluding the start node but including the end node.
                int longestPath = Math.max(0, longestPathLast.getD());
                subPathLengths.add(longestPath);
            }
        }
    }

    /**
     * Does Breadth First Search.
     *
     * Resetting the graph is done by keeping track of all vertices this function updated before in cacheBFS.
     * This reduces the amount of reset operations it has to do.
     *
     * @param v The starting vertex
     * @return The last visited vertex
     */
    public Vertex BFS(Vertex v) {
        for(Vertex x : cacheBFS) {
            x.setColor(Color.WHITE);
            x.setD(Integer.MAX_VALUE);
            x.setPi(null);
        }
        cacheBFS = new ArrayList<>();

        Queue<Vertex> queue = new LinkedList<>();

        v.setColor(Color.GRAY);
        v.setD(0);
        queue.add(v);

        //Used to return lastVisited vertex
        Vertex currentVertex = null;

        while(!queue.isEmpty()) {
            currentVertex = queue.remove();
            currentVertex.setVisited(true);
            cacheBFS.add(currentVertex);

            for(Vertex w : currentVertex.getAdjacency()) {
                if(w.getColor() == Color.WHITE) {
                    w.setColor(Color.GRAY);
                    w.setD(currentVertex.getD() + 1);
                    w.setPi(currentVertex);
                    queue.add(w);
                }
            }
            currentVertex.setColor(Color.BLACK);
        }

        return currentVertex;
    }

    public List<Integer> getSubPathLengths() {
        return subPathLengths;
    }
}
