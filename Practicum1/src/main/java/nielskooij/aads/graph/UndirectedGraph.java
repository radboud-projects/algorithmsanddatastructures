package nielskooij.aads.graph;

public class UndirectedGraph {

    private int V;
    private Vertex[] vertices;

    public UndirectedGraph(int V) {
        this.V = V;
        this.vertices = new Vertex[V];

        for(int i = 0; i < V; i++) {
            vertices[i] = new Vertex(i);
        }
    }

    public void addEdge(int src, int dest) {
        vertices[src].addAdjecent(vertices[dest]);
        vertices[dest].addAdjecent(vertices[src]);
    }

    public Vertex[] getVertices() {
        return vertices;
    }

    public int getVerticeAmount() {
        return V;
    }

    public Vertex getVertex(int number) {
        return vertices[number];
    }

}
