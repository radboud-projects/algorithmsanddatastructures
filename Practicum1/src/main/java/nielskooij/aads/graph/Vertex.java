package nielskooij.aads.graph;

import java.awt.*;
import java.util.LinkedList;

public class Vertex {

    private final int number;
    private LinkedList<Vertex> adjacency = new LinkedList<>();

    private Color color = Color.WHITE;
    private int d = -1;
    private Vertex pi = null;

    private boolean isVisited = false;

    public Vertex(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public Vertex getPi() {
        return pi;
    }

    public void setPi(Vertex pi) {
        this.pi = pi;
    }

    public LinkedList<Vertex> getAdjacency() {
        return adjacency;
    }

    public void setAdjacency(LinkedList<Vertex> adjacency) {
        this.adjacency = adjacency;
    }

    public void addAdjecent(Vertex vertex) {
        adjacency.add(vertex);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        this.isVisited = visited;
    }
}
