package nielskooij.aads;

import nielskooij.aads.graph.UndirectedGraph;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        Parser parser = new Parser(System.in);
        UndirectedGraph g = parser.parseInput();

        PiAlgorithm algorithm = new PiAlgorithm();
        int result = algorithm.calculateLongestPath(g);
        System.out.println(result);
    }

}


