package nielskooij.aads;

import nielskooij.aads.graph.UndirectedGraph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Parser {

    private BufferedReader reader;

    private UndirectedGraph g;
    int connectionLines = Integer.MAX_VALUE;

    public Parser(InputStream input) {
        reader = new BufferedReader(new InputStreamReader(input));
    }

    /**
     * Parse input and build a graph from it.
     * @return The UndirectedGraph
     * @throws IOException
     */
    public UndirectedGraph parseInput() throws IOException {
        String currentLine;

        readFirstLine(reader.readLine());

        while(connectionLines > 0 && (currentLine = reader.readLine()) != null) {
            String[] parts = currentLine.split(" ");

            parseEdge(parts);
            connectionLines--;
        }

        reader.close();
        return g;
    }

    private void readFirstLine(String line) {
        String[] parts = line.split(" ");

        g = new UndirectedGraph(Integer.parseInt(parts[0]));
        connectionLines = Integer.parseInt(parts[1]);
    }

    private void parseEdge(String[] parts) {
        g.addEdge(
                Integer.parseInt(parts[0]),
                Integer.parseInt(parts[1])
        );
    }

    /**
     * Parse an output file from the samples.
     * @return The expected output
     * @throws IOException
     */
    public int parseOutput() throws IOException {
        return Integer.parseInt(reader.readLine());
    }

}
