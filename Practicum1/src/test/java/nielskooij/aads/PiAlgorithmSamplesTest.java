package nielskooij.aads;

import nielskooij.aads.graph.UndirectedGraph;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class PiAlgorithmSamplesTest {

    private PiAlgorithm algorithm;

    @Test
    public void testSmallSamples() throws IOException {
        for(int i = 1; i < 10; i++) {
            UndirectedGraph g = new Parser(SampleTestHelper.getSmallSampleInput(i)).parseInput();

            algorithm = new PiAlgorithm();
            int result = algorithm.calculateLongestPath(g);

            int actual = new Parser(SampleTestHelper.getSmallSampleOutput(i)).parseOutput();
            assertEquals("Failed small test nr" + i, actual, result);
        }
    }

    @Test
    public void testBigSamples() throws IOException {
        for(int i = 1; i < 11; i++) {
            UndirectedGraph g = new Parser(SampleTestHelper.getBigSampleInput(i)).parseInput();

            algorithm = new PiAlgorithm();
            int result = algorithm.calculateLongestPath(g);

            int actual = new Parser(SampleTestHelper.getBigSampleOutput(i)).parseOutput();
            assertEquals("Failed big test nr" + i, actual, result);
        }
    }

    @Test
    public void testServerSamples() throws IOException {
        String[] categories = new String[]{"A", "B", "C"};

        for(String category : categories) {
            int counter = 1;
            File input = SampleTestHelper.getSampleInput("sample-" + category + "." + counter);

            while(input.exists()) {
                UndirectedGraph g = new Parser(new FileInputStream(input)).parseInput();

                algorithm = new PiAlgorithm();
                int result = algorithm.calculateLongestPath(g);

                int actual = new Parser(new FileInputStream(SampleTestHelper.getSampleOutput("sample-" + category + "." + counter))).parseOutput();
                assertEquals("Failed test for : sample-" + category + "." + counter , actual, result);

                counter++;
                input = SampleTestHelper.getSampleInput("sample-" + category + "." + counter);
            }
        }
    }

    @Test
    public void testDebugSingle() throws IOException {
        String fileName = "sample-C.21";

        UndirectedGraph g = new Parser(new FileInputStream(SampleTestHelper.getSampleInput(fileName))).parseInput();

        algorithm = new PiAlgorithm();
        algorithm.calculateLongestPath(g);

        assertTrue(true);
    }

}
