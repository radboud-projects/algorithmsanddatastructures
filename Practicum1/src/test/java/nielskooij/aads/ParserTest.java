package nielskooij.aads;

import nielskooij.aads.graph.UndirectedGraph;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class ParserTest {

    private Parser parser;

    @Before
    public void setup() {
        parser = null;
    }

    @Test
    public void smallInput() throws IOException {
        parser = new Parser(SampleTestHelper.getSmallSampleInput(6));

        UndirectedGraph g = parser.parseInput();

        int[] expected = new int[]{ 1, 2, 2, 1, 1, 2, 1, 1, 2, 1};

        assertEquals(10, g.getVerticeAmount());
        for(int i = 0; i < g.getVerticeAmount(); i++) {
            assertEquals(expected[i], g.getVertex(i).getAdjacency().size());
        }
    }

    @Test
    public void smallOutput() throws IOException {
        parser = new Parser(SampleTestHelper.getSmallSampleOutput(6));

        assertOutput(parser, 3);
    }

    @Test
    public void bigOutput() throws IOException {
        parser = new Parser(SampleTestHelper.getBigSampleOutput(1));

        assertOutput(parser, 1);
    }

    private void assertOutput(Parser parser, int expected) throws IOException {
        int actual = parser.parseOutput();

        assertEquals(expected, actual);
    }
}
