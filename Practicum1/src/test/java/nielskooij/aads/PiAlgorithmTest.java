package nielskooij.aads;

import nielskooij.aads.graph.UndirectedGraph;
import nielskooij.aads.graph.Vertex;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class PiAlgorithmTest {

    private static final int SAMPLE_NUMBER = 11;

    private PiAlgorithm algorithm;
    private UndirectedGraph g;

    @Before
    public void setup() throws IOException {
        algorithm = new PiAlgorithm();
        g = new Parser(SampleTestHelper.getSmallSampleInput(SAMPLE_NUMBER)).parseInput();
    }

    @Test
    public void BFSTest() {
        Vertex v = algorithm.BFS(g.getVertex(3));

        assertTrue(v.getNumber() == 0 || v.getNumber() == 2);
    }

    @Test
    public void BFSTest2() {
        Vertex v = algorithm.BFS(g.getVertex(7));

        assertTrue(v.getNumber() == 5);
    }

    @Test
    public void pathAlgorithmTest() {
        algorithm.pathAlgorithm(g);

        List<Integer> subPathLengths = algorithm.getSubPathLengths();
        assertEquals(2, subPathLengths.size());

        assertEquals(3, subPathLengths.get(0).intValue());
        assertEquals(2, subPathLengths.get(1).intValue());
    }

    @Test
    public void calculateLongestPathTest() {
        int length = algorithm.calculateLongestPath(g);
        assertEquals(3, length);
    }

}
