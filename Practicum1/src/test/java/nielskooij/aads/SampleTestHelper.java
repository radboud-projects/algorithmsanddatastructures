package nielskooij.aads;

import java.io.*;

public class SampleTestHelper {

    private static File sampleDir = new File(SampleTestHelper.class.getResource("/samples").getFile());

    public static InputStream getSmallSampleInput(int index) throws FileNotFoundException {
        File sample = new File(sampleDir, "small_" + index + ".in");
        return new FileInputStream(sample);
    }

    public static InputStream getSmallSampleOutput(int index) throws FileNotFoundException {
        File sample = new File(sampleDir, "small_" + index + ".out");
        return new FileInputStream(sample);
    }

    public static File getSampleInput(String name) {
        return new File(sampleDir, name + ".in");
    }

    public static File getSampleOutput(String name) {
        return new File(sampleDir, name + ".out");
    }

    public static InputStream getBigSampleInput(int index) throws FileNotFoundException {
        File sample = new File(sampleDir, "big_" + index + ".in");
        return new FileInputStream(sample);
    }

    public static InputStream getBigSampleOutput(int index) throws FileNotFoundException {
        File sample = new File(sampleDir, "big_" + index + ".out");
        return new FileInputStream(sample);
    }

}
