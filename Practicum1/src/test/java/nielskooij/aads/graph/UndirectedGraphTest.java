package nielskooij.aads.graph;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UndirectedGraphTest {

    private UndirectedGraph undirectedGraph;

    @Test
    public void testaddVertex() {
        undirectedGraph = new UndirectedGraph(3);

        undirectedGraph.addEdge(0, 1);
        undirectedGraph.addEdge(1, 2);

        assertEquals(1, undirectedGraph.getVertex(0).getAdjacency().size());
        assertEquals(2, undirectedGraph.getVertex(1).getAdjacency().size());
    }
}
